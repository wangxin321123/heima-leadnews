package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;


/**
 * ApUserService
 *
 * @author liudo
 * @version 1.0
 * @project heima-leadnews
 * @date 2023/10/25 15:46:27
 */
public interface ApUserService extends IService<ApUser> {

    ResponseResult login(LoginDto loginDto);
}
