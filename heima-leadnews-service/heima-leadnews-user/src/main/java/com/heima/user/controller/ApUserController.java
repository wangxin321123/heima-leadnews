package com.heima.user.controller;


import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.user.service.ApUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/user/api/v1/login")
public class ApUserController {

    @Autowired
    private ApUserService apUserService;

    @PostMapping("/login auth")
    public ResponseResult loginAuth(@RequestBody LoginDto loginDto){
        return  apUserService.login(loginDto);
    }
}
