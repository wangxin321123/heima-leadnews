package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liudo
 * @version 1.0
 * @project heima-leadnews
 * @date 2023/10/25 15:52:59
 */
@Mapper
public interface ApUserMapper extends BaseMapper<ApUser> {
}
